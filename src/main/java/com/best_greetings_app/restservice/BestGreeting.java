package com.best_greetings_app.restservice;

public class BestGreeting {
    private final long id;
    private final String content;

    public BestGreeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}

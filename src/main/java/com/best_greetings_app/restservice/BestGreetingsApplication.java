package com.best_greetings_app.restservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestGreetingsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BestGreetingsApplication.class, args);
    }

//    browser request:
//    http://localhost:8080/bestGreeting?name=Arturs
}

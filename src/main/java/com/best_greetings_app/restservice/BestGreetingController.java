package com.best_greetings_app.restservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class BestGreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/bestGreeting")
    public BestGreeting bestGreeting(@RequestParam(value = "name", defaultValue = "World")
                                             String name) {
        return new BestGreeting(counter.incrementAndGet(), String.format(template, name));
    }
}
